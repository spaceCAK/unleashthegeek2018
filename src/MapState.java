
/*
 * 
 */
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class MapState {
	private Vector basePosition;
	private List<Heroe> heroes;
	private List<Enemy> enemies;
	private List<Monster> monsters;
	public int availableMana;
	public int health;
	public int otherAvailableMana;
	public int otherHealth;

	public static Vector MAP_SIZE = Vector.createCVector(17630, 9000);
	public static int MONSTER_ATTACK_RADIUS = 5000;
	public static int MONSTER_DAMAGE_RADIUS = 300;
	public static int MONSTER_SPEED = 400;

	List<Vector> positions;

	public MapState(Vector basePosition) {
		this.basePosition = basePosition;
			positions = Arrays.asList(Vector.createCVector(13500, 5500), Vector.createCVector(3500, 2000),
					Vector.createCVector(2000, 3500), MAP_SIZE.sub(Vector.createCVector(13500, 5500)),
					MAP_SIZE.sub(Vector.createCVector(3500, 2000)), MAP_SIZE.sub(Vector.createCVector(2000, 3500)));
	}

	public Vector getPosition(int heroeId){
		return positions.get(heroeId);
	}
	public List<Heroe> getDefensiveHeroes() {
		return heroes.subList(1, 3);
	}

	public Heroe getAttackingHeroe() {
		return heroes.get(0);
	}

	public Vector getOwnBase() {
		return basePosition;
	}

	public Vector getEnemyBase() {
		if (basePosition.getX() == 0) {
			return MAP_SIZE;
		}
		return Vector.createCVector(0, 0);
	}

	public List<Heroe> getHeroes() {
		return heroes;
	}

	public void setHeroes(List<Heroe> heroes) {
		this.heroes = heroes;
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<Enemy> enemies) {
		this.enemies = enemies;
	}

	public List<Monster> getMonsters() {
		return monsters;
	}

	public void setMonsters(List<Monster> monsters) {
		this.monsters = monsters;
	}

	public MapState(MapState toCopy) {
		heroes = new ArrayList<>();
		for (Heroe heroe : toCopy.heroes) {
			heroes.add(new Heroe(heroe));
		}
		monsters = new ArrayList<>();
		for (Monster monster : toCopy.monsters) {
			monsters.add(new Monster(monster));
		}
		enemies = new ArrayList<>();
		for (Enemy enemy : toCopy.enemies) {
			enemies.add(new Enemy(enemy));
		}

		basePosition = toCopy.basePosition;
		availableMana = toCopy.availableMana;
		health = toCopy.health;
		positions = toCopy.positions;
	}
	
	

	public double calculateValue() {
		return calculateDefenseValue() + calculateAttackValue() + availableMana;
	}

	private double calculateAttackValue() {
		double value = 0;
		for (Monster m : monsters) {
			double monsterValue = m.health;
			if (m.willHit(15, getEnemyBase())) {
				monsterValue *= 5;
				if (m.shieldLife > 0) {
					monsterValue *= 10;
					if (m.willHit(m.shieldLife, getEnemyBase())) {
						monsterValue *= 2;
					}
				}
			}
			else{
				monsterValue += calculateDistancePoints(m.position.distance(getEnemyBase()), 15);
			}
			value += monsterValue;
		}
		Heroe heroe = getAttackingHeroe();
		double heroeValue = 0;
		heroeValue -= heroe.position.distance(getPointsOfInterest(heroe));
		for (Monster m : monsters) {
			double distance = heroe.position.distance(m.position);
			// don't consider this Monster if we can't reach it in less than 4
			// steps
			// don't consider this monster if it will hit the enemy
			if (distance > 10 * 800 || m.willHit(15, getEnemyBase())) {
				continue;
			}
			heroeValue += calculateDistancePoints(distance, 5);
		}
		value += heroeValue;

		return value;
	}

	private double calculateDistancePoints(double distance, double maxPoints) {
		return maxPoints - (distance / 800);
	}

	private double calculateDefenseValue() {
		double res = 0;
		// nb of clusters
		KMeans<Monster> kMeans = new KMeans<Monster>();
		List<EntityCloud<Monster>> pureDefenceClusters = kMeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
		// pureDefenceClustersNumber
		res += pureDefenceClusters.size();
		// windDefenceClustersNumber
		res += kMeans.findKClusters(monsters, Utils.WIND_RADIUS).size();

		for (EntityCloud<Monster> cluster : pureDefenceClusters) {
			res += computeClusterRisk(cluster);
		}
		// distance to preferred position
		res += computeKeepLineError();
		return -res;
	}

	private double computeClusterRisk(EntityCloud<Monster> cluster) {
		// health
		int bestHealth = Utils.getBestHealth(monsters);
		Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, getOwnBase());
		// defensive capability
		double heroeDistance = Utils.findClosest(getDefensiveHeroes(), closestMonster.getKey().position).getValue();
		// enemy attack capability
		double attackerDistance = Utils.findClosest(getEnemies(), cluster.getCenter()).getValue();

		double res = 0;
		res += Math.min(0, heroeDistance - Utils.ATTACK_RADIUS*.8)*.001;
		res += Utils.WIND_RADIUS*1.1 >= attackerDistance ? 1 : 0;
		res += Math.pow(3000 / closestMonster.getValue(), 4);
		res += Math.pow(2000 / closestMonster.getValue(), 4);
		return res*bestHealth;
	}

	private double computeKeepLineError() {
		List<Heroe> defensers = getDefensiveHeroes();
		double res = (Math.max(0, defensers.get(0).position.distance(defensers.get(1).position) - 3000))*.001;
		for (Heroe heroe : getHeroes()) {
			if (heroe.isAttacker) continue;
			res += Math.pow(heroe.position.distance(getPointsOfInterest(heroe))*.001, 2)*.5;
		}
		return res;
	}

	public MapState calculateMapStateShield(Entity entity) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (entity.id == m.id) {
				m.shieldLife = 12;
				break;
			}
		}
		for (Heroe h : copy.getHeroes()) {
			if (entity.id == h.id) {
				h.shieldLife = 12;
				break;
			}
		}
		for (Enemy e : copy.getEnemies()) {
			if (entity.id == e.id) {
				e.shieldLife = 12;
				break;
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateControl(Entity entity, Vector targetPosition) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (entity.id == m.id) {
				m.speed = targetPosition.sub(m.position).normalize(400);
				break;
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateWind(Heroe heroe, Vector windDirection) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (m.getFuturePosition().distance(heroe.position) < 1280 && m.shieldLife == 0) {
				m.position = m.position.add(windDirection.normalize(2200));
			}
		}
		for (Enemy e : copy.getEnemies()) {
			if (e.getFuturePosition().distance(heroe.position) < 1280 && e.shieldLife == 0) {
				e.position = e.position.add(windDirection.normalize(2200));
			}
		}
		for (Heroe h : copy.getHeroes()) {
			if (h.getFuturePosition().distance(heroe.position) < 1280 && h.shieldLife == 0 && h.id != heroe.id) {
				h.position = h.position.add(windDirection.normalize(2200));
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateMove(Heroe heroe, Vector futurePosition) {
		MapState copy = new MapState(this);
		for (Heroe h : copy.getHeroes()) {
			if (h.id == heroe.id) {
				h.position = h.position
						.add(futurePosition.normalize(futurePosition.size() > 800 ? 800 : futurePosition.size()));
				break;
			}
		}
		return copy;
	}

	Vector getPointsOfInterest(Heroe heroe) {
		return getPosition(heroe.id);
	}
}
