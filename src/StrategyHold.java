/**
 *
 */
import java.util.ArrayList;
import java.util.List;

public class StrategyHold extends Strategy {
  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
    for (Heroe heroe : mapState.getHeroes()) {
      res.add(new HoldPosition(heroe, mapState, ""));
    }
    return res;
  }
}
