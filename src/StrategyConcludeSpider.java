/*
 * 
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Strategy: when spider is close enough to enemy base, make sure it reaches destination
 */
public class StrategyConcludeSpider extends Strategy {

  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    List<CandidateAction> candidates = new ArrayList<>();
    for (Monster monster : mapState.getMonsters()) {
      if (monster.shieldLife > 0) continue;
      double distanceToEnemy = monster.position.distance(mapState.getEnemyBase());


    }
    return candidates;
  }
}
