/**
 * 
 * @author cpichery
 *
 */


public class SpellWind extends Spell {

	private Vector direction;

	SpellWind(Heroe heroe, double value, Vector direction, String explanation) {
		super(heroe, null, value, explanation);
		this.direction = direction;
	}

	public void send() {
		System.out.println("SPELL WIND " + (int) direction.getX() + " " + (int) direction.getY() + " " + heroe.id + " " + explanation);
	}

}
