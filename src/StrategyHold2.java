
/**
 *
 */
import java.util.ArrayList;
import java.util.List;

public class StrategyHold2 extends Strategy {
	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
		for (Heroe heroe : mapState.getHeroes()) {
			MapState newMapState = mapState.calculateMapStateMove(heroe, mapState.getPosition(heroe.id));
			res.add(new Move(heroe, newMapState.calculateValue() - mapState.calculateValue(),
					mapState.getPosition(heroe.id), "hold the line!"));
		}
		return res;
	}
}
