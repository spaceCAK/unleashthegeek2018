/*
 * 
 */
import java.util.List;

public abstract class Spell extends CandidateAction {
  Entity target;

  Spell(Heroe heroe, Entity target, double value, String explanation) {
    super(heroe, 10, value, explanation);
    this.target = target;
  }

  public boolean isSpell() {return true;}
  public boolean isMove() {return false;}
}
