/**
 *
 */

import java.util.Scanner;

public class Enemy extends Entity {
	public Enemy(Scanner in, int id) {
		super(in);
		this.id = id;
	}

	public Enemy(Enemy toCopy) {
		super(toCopy);
	}

}
