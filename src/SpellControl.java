/*
 * 
 */
import java.util.List;

public class SpellControl extends Spell {

  Vector direction;

  SpellControl(Heroe hero, Entity targetMonster, Vector direction, double value, String explanation) {
    super(hero, targetMonster, value, explanation);
    this.direction = direction;
  }

  public void send() {
		System.out.println(
				"SPELL CONTROL " + target.id + " " + (int) direction.getX() + " " + (int) direction.getY() + " " + heroe.id + " " + explanation);
  }

}
