/**
*
*/

import java.util.*;
import java.io.*;

import java.math.*;

/**
 * Auto-generated code below aims at helping you parse the standard input
 * according to the problem statement.
 **/
public class Player {

	public static MapState latestMapState;

	public static void main(String args[]) throws IOException {
		Scanner in = new Scanner(System.in);
		latestMapState = new MapState(Vector.createCVector(in.nextInt(), in.nextInt()));

		int heroesPerPlayer = in.nextInt();

		// game loop
		while (true) {
			for (int i = 0; i < 2; i++) {
				if (i == 0) {
					latestMapState = new MapState(latestMapState.getOwnBase());
					latestMapState.health = in.nextInt();
					latestMapState.availableMana = in.nextInt();
				} else {
					latestMapState.otherHealth = in.nextInt();
					latestMapState.otherAvailableMana = in.nextInt();
				}
			}
			int PlayerCount = in.nextInt();
			List<Monster> monsters = new ArrayList<>();
			List<Heroe> heroes = new ArrayList<>();
			List<Enemy> enemies = new ArrayList<>();
			boolean isAttacker = true;

			for (int i = 0; i < PlayerCount; i++) {
				int id = in.nextInt();
				int type = in.nextInt();
				switch (type) {
				case 0:
					monsters.add(new Monster(in, id, latestMapState));
					break;
				case 1:
					// only the 1st hero is attacker
					heroes.add(new Heroe(in, id, latestMapState, isAttacker));
					isAttacker = false;
					break;
				default:
					enemies.add(new Enemy(in, id));
					break;
				}
			}
			latestMapState.setEnemies(enemies);
			latestMapState.setMonsters(monsters);
			latestMapState.setHeroes(heroes);

			act(latestMapState);

			if (false)
				break;
		}
	}

	private static void act(MapState mapState) {
		for (Monster monster : mapState.getMonsters()) {
			monster.setLevelOfDanger(latestMapState.getOwnBase());
		}
		// find candidate actions
		List<Strategy> strategies = Arrays.asList(/*new StrategyConcludeSpider(), */
						new StrategyHold2(),
						new StrategyAttack2(),
						new StrategyDefence2());
		List<CandidateAction> candidateActions = new ArrayList<>();
		for (Strategy strategy : strategies) {
			candidateActions.addAll(strategy.buildCandidateActions(mapState));
		}

		// decide which actions to take
		List<CandidateAction> actionsToTake = (new CandidateActionChooser(mapState)).choose(candidateActions);
		for(CandidateAction action : actionsToTake){
			action.send();
		}
	}

}