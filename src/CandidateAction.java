/*
 * 
 */
public abstract class CandidateAction implements Comparable<CandidateAction> {

  protected final String explanation;
  Heroe heroe;
  double value;
  int manaCost;

  public CandidateAction(Heroe heroe, int manaCost, double value, String explanation) {
    if (heroe == null) throw new IllegalArgumentException("Action cannot be done on null heroe:" + this.getClass().getSimpleName());
    this.heroe = heroe;
    this.manaCost = manaCost;
    this.value = value;
    this.explanation = explanation;
  }

  public abstract boolean isSpell();
  public abstract boolean isMove();

  public int compareTo(CandidateAction otherCandidateAction) {
    return (int)(value - otherCandidateAction.value);
  }

  public abstract void send();
  
  public String toString(){
	  return this.getClass().getName() + " H" + (this.heroe == null ? null : this.heroe.id ) + " V" + this.value + " M" + this.manaCost + " " + explanation;
  }

}
