/**
 *
 */



/**
 * 
 */
public class HoldPosition extends CandidateAction {

	Vector position;

	public HoldPosition(Heroe heroe, MapState mapState, String explanation) {
		super(heroe, 0, heroe.position.distance(mapState.getPointsOfInterest(heroe))*.01, explanation);
		position = mapState.getPointsOfInterest(heroe);
	}

	@Override
	public boolean isSpell() {
		return false;
	}

	@Override
	public boolean isMove() {
		return true;
	}
	

	public void send() {
		System.out.println("MOVE " + (int) position.getX() + " " + (int) position.getY() + " " + heroe.id + " " + explanation);
	}

}
