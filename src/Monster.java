
/**
 *
 */
import java.util.Scanner;

public class Monster extends Entity {
	double levelOfDanger;
	private MapState state;

	Monster(Scanner in, int id, MapState state) {
		super(in);
		this.id = id;
		this.state = state;
	}

	public void setLevelOfDanger(Vector basePosition) {
		levelOfDanger = 1;
		double distanceToBase = basePosition.distance(this.position);
		levelOfDanger /= distanceToBase / 1000;
		if (distanceToBase < 8000) {
			levelOfDanger *= 10;
		}
		if (distanceToBase < 5000) {
			levelOfDanger *= 10;
		}
	}

	public boolean isMonsterNearBorder() {
		return (this.position.getX() < 2000 || this.position.getX() > 17630 - 2000 || this.position.getY() < 2000
				|| this.position.getY() > 9000 - 2000);
	}

	public boolean isTargettingEnemyBase() {
		return nearBase == 1 && threatFor == 2;
	}

	public boolean isTargettingOurBase() {
		return nearBase == 1 && threatFor == 1;
	}

	public boolean isThreatForUs() {
		return threatFor == 1;
	}

	public boolean isThreatForEnemy() {
		return threatFor == 2;
	}

	public Monster(Monster toCopy) {
		super(toCopy);
		levelOfDanger = toCopy.levelOfDanger;
	}

	public boolean willHit(int iteration, Vector pos) {
		for(int i = 1; i < iteration + 1; i++){
			Vector futurePos = this.position.add(this.speed.scalar(i));
			if(futurePos.getX() > 0 && futurePos.getY() > 0 && futurePos.getY() < 9000 && futurePos.getX() < 17360){
				if(futurePos.distance(pos) < 5000){
					return true;
				}
			}
			else{
				return false;
			}
		}
		return false;
	}
}