/*
 * 
 */
import java.util.ArrayList;
import java.util.List;

/**
 * Strategy: when spiders are getting dangerous, try to eject them out of the map
 */
public class StrategyEjectSpider extends Strategy {


  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    return new ArrayList<>();
  }
}
