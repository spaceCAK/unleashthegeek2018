/**
 * 
 * @author cpichery
 *
 */
public class SpellShield extends Spell {

	SpellShield(Heroe heroe, Entity target, double value, String explanation) {
		super(heroe, target, value, explanation);
	}

	public void send() {
		System.out.println("SPELL SHIELD " + target.id + " " + heroe.id + " " + explanation);
	}

}
