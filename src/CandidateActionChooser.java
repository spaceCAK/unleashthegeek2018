
/*
 * 
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class CandidateActionChooser {

	private final MapState mapState;
	private HashSet<Heroe> busyHeroes = new HashSet<>();

	CandidateActionChooser(MapState mapState) {
		this.mapState = mapState;
	}

	public List<CandidateAction> choose(List<CandidateAction> candidateActions) {
		Collections.sort(candidateActions);
		Collections.reverse(candidateActions);
		List<CandidateAction> res = new ArrayList<>();
		Iterator<CandidateAction> iterator = candidateActions.iterator();
		while (iterator.hasNext() && res.size() < 3) {
			proposeAction(iterator.next(), res);
		}
		return sortPerHeroe(res);
	}

	private List<CandidateAction> sortPerHeroe(List<CandidateAction> res) {
		List<CandidateAction> out = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			for (CandidateAction action : res) {
				if (action.heroe.id == i) {
					out.add(action);
					break;
				}
			}
		}
		return out;
	}

	private void proposeAction(CandidateAction action, List<CandidateAction> res) {
		if (action.manaCost > mapState.availableMana)
			return;
		if (action.heroe == null) {
			// no constraint on heroe
			res.add(action);
			mapState.availableMana -= action.manaCost;
		} else {
			if (!busyHeroes.contains(action.heroe)) {
				// heroe is available
				res.add(action);
				mapState.availableMana -= action.manaCost;
				busyHeroes.add(action.heroe);
			}
		}
	}
}
