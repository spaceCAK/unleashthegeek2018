/**
 *
 */

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class KMeans<T extends Entity> {

  public Map<Vector, List<T>> kmeansPlusPlus(List<T> entities, int k) {
    Map<Vector, List<T>> clusters = new HashMap<>();
    // we decide centroid #1
    clusters.put(entities.get(0).position, new ArrayList<T>());
    for (int i = 1 ; i < k ; i++) {
      // find centroid #k
      Vector candidate = null;
      double distanceToCandidate = -1;
      for (T entity : entities) {
        // find a candidate among entities
        Pair<Vector, Double> closestCenter = Utils.findClosestPosition(clusters.keySet(), entity.position);
        if (closestCenter.getValue() > distanceToCandidate) {
          candidate = entity.position;
          distanceToCandidate = closestCenter.getValue();
        }
      }
      if (candidate == null) throw new IllegalStateException("Why null in k-means++?");
      clusters.put(candidate, new ArrayList<T>());
    }
    return clusters;
  }

  private void buildClusters(List<T> entities, Map<Vector, List<T>> clusters) {
    for (T entity : entities) {
      Pair<Vector, Double> center = Utils.findClosestPosition(clusters.keySet(), entity.position);
      clusters.get(center.getKey()).add(entity);
    }
  }

  /**
   * return null if no need to improve further
   */
  private Map<Vector, List<T>> improveCenters(Map<Vector, List<T>> clusters) {
    double positionSplit = 0;
    Map<Vector, List<T>> newCenters = new HashMap<>();
    for (Map.Entry<Vector, List<T>> cluster : clusters.entrySet()) {
      List<T> clusterElements = cluster.getValue();
      Vector[] positions = new Vector[clusterElements.size()];
      for (int i = 0 ; i < clusterElements.size() ; i++) {
        positions[i] = clusterElements.get(i).position;
      }
      Vector newCenter = Vector.centroid(positions);
      positionSplit += newCenter.distance(cluster.getKey());
      newCenters.put(newCenter, new ArrayList<T>());
    }
    positionSplit /= clusters.size();
    if (positionSplit < 5) return null;
    return newCenters;
  }

  public Map<Vector, List<T>>  findClusters(List<T> entities, int k) {
    if (entities.isEmpty()) return new HashMap<>();
    Map<Vector, List<T>> newClusters = kmeansPlusPlus(entities, k);
    Map<Vector, List<T>> clusters = null;
    int counter = 0;
    do {
      clusters = newClusters;
      buildClusters(entities, clusters);
      newClusters = improveCenters(clusters);
      counter++;
    } while (newClusters != null && counter < 100);
    return clusters;
  }

  public List<EntityCloud<T>> findKClusters(List<T> entities, double maxAcceptedDiameter) {
    List<EntityCloud<T>> res;
    Map<Vector, List<T>> clusters;
    double maxDiameter;
    int k = 1;
    do {
      clusters = findClusters(entities, k);
      k++;
      maxDiameter = 0;
      res = new ArrayList<>(clusters.size());
      for (List<T> clusterElements : clusters.values()) {
        EntityCloud entityCloud = new EntityCloud<T>(clusterElements);
        res.add(entityCloud);
        maxDiameter = Math.max(maxDiameter, entityCloud.getDiameter());
      }
    } while (maxDiameter > maxAcceptedDiameter && k <= entities.size());
    return res;
  }

}
