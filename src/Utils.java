/**
 * 
 */

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 */
public class Utils {

  public static int ATTACK_RADIUS = 800;
  public static int WIND_RADIUS = 1280;
  public static int CONTROL_RADIUS = 2200;
  public static int SHIELD_RADIUS = 2200;
  public static int MONSTER_SPEED = 400;

  static public int getBestHealth(List<Monster> monsters) {
    int bestHealth = 0;
    for (Monster monster : monsters) {
      bestHealth = Math.max(monster.health, bestHealth);
    }
    return bestHealth;
  }

  static public <T extends Entity> Pair<T, Double> findClosest(Collection<T> entities, Vector referencePosition) {
    T candidate = null;
    double distance = 100000;
    double candidateDistance;
    for (T entity : entities) {
      candidateDistance = entity.position.distance(referencePosition);
      if (candidateDistance < distance) {
        distance = candidateDistance;
        candidate = entity;
      }
    }
    return new Pair<>(candidate, distance);
  }

  static public Pair<Vector, Double> findClosestPosition(Collection<Vector> positions, Vector referencePosition) {
    Vector candidate = null;
    double distance = 100000;
    double candidateDistance;
    for (Vector position : positions) {
      candidateDistance = position.distance(referencePosition);
      if (candidateDistance < distance) {
        distance = candidateDistance;
        candidate = position;
      }
    }
    return new Pair<>(candidate, distance);
  }

  static public List<Heroe> getOtherDefensiveHeroes(MapState mapState, Heroe heroe) {
    List<Heroe> res = new ArrayList<>();
    for (Heroe candidateHeroe : mapState.getHeroes()) {
      if (candidateHeroe != heroe && !candidateHeroe.isAttacker) {
        res.add(candidateHeroe);
      }
    }
    return res;
  }

}
