import java.util.List;

/**
 *
 */
public class Vector implements Cloneable {

  private static int MAX = 1000000;

  public enum Coord {X, Y;}

  private double x; double y; double r; double theta;
  private Vector(double x, double y, double r, double theta) {this.x = x; this.y = y; this.r = r; this.theta = theta;}
  double getX() {return x;} double getY() {return y;} double getR() {return r;} double getTheta() {return theta;}
  double getCoord(Coord coord) {switch(coord) {case X: return x; case Y: return y;} throw new IllegalArgumentException("Coord cannot be null");}
  double size()               {return Math.abs(r);}
  Vector add(Vector o)        {return createCVector(x+o.x, y+o.y);}
  Vector scalar(double l)     {return createCVector(l*x, l*y);}
  double scalar(Vector o)     {return x*o.x+y*o.y;}
  double cross(Vector o)      {return x*o.y-y*o.x;}
  Vector sub(Vector o)        {return add(o.scalar(-1));}
  Vector normalize(double s)  {return createPVector(s, theta);}
  Vector rotateRadians(double radians) {return createPVector(r, theta + radians);}
  static Vector createCVector(double x, double y) {return new Vector(x, y, Math.sqrt(x*x+y*y), (Math.sqrt(x*x+y*y)<0.01)?0:Math.atan2(y,x));}
  static Vector createPVector(double r, double theta) {return new Vector(r*Math.cos(theta), r*Math.sin(theta), r, theta);}
  static Vector nullVector()  {return new Vector(0,0,0,0);}
  @Override
  public String toString() {return "("+x+","+y+")";}
  public String toPString() {return "("+r+","+theta+")";}
  public double distance(Vector otherPos){return this.sub(otherPos).size();}

  @Override
  public Vector clone() {
    return Vector.createCVector(getX(), getY());
  }

  public static Vector centroid(Vector... points) {
    Vector res = nullVector();
    for (int i = 0 ; i < points.length ; i++) {
      res = res.add(points[i]);
    }
    return res.scalar(1./points.length);
  }

  public static Vector boxCenter(Vector... points) {
    if (points.length == 0) return nullVector();
    double minX = MAX;
    double maxX = -MAX;
    double minY = MAX;
    double maxY = -MAX;
    for (int i = 0 ; i < points.length ; i++) {
      Vector point = points[i];
      minX = Math.min(minX, point.x);
      maxX = Math.max(maxX, point.x);
      minY = Math.min(minY, point.y);
      maxY = Math.max(maxY, point.y);
    }
    return createCVector(.5*(minX+maxX), .5*(minY+maxY));
  }

}
