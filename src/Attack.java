
/**
 * 
 */

import java.util.ArrayList;
import java.util.List;

public class Attack extends Strategy {

	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		List<CandidateAction> actions = new ArrayList<>();
		for (Heroe heroe : mapState.getHeroes()) {
			if (heroe.isAttacker) {
				List<Monster> monsters = heroe.getMonstersToAttack(mapState.getMonsters());
				int index = 0;
				for (Monster monster : monsters) {
						if(mapState.availableMana > 50 && monster.shieldLife == 0 && monster.willHit(5, mapState.getEnemyBase()) && monster.position.distance(heroe.position) < 2200){
							actions.add(new SpellShield(heroe, monster, 100, "Sayonara!"));
						}
						if (mapState.availableMana > 50 && monster.shieldLife == 0 && monster.position.distance(heroe.position) < 2200) {
							actions.add(new SpellControl(heroe, monster, mapState.getEnemyBase(), monster.health, "good boi"));
						}
						if (!monster.isTargettingEnemyBase()) {
							actions.add(new Move(heroe, monster.health - index * 2, monster.position, "ramdam"));
						}
					
					index++;
				}
			}
		}
		return actions;
	}

}
