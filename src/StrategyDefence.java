/**
 *
 */
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StrategyDefence extends Strategy {

  private final KMeans<Monster> kmeans;

  public StrategyDefence() {
    kmeans = new KMeans<Monster>();
  }

  private void sortByProximity(List<EntityCloud<Monster>> entityClouds, final MapState mapState) {
    Collections.sort(entityClouds, new Comparator<EntityCloud<Monster>>() {
      @Override
      public int compare(EntityCloud<Monster> o1, EntityCloud<Monster> o2) {
        return (int) (o1.getCenter().distance(mapState.getOwnBase()) - o2.getCenter().distance(mapState.getOwnBase()));
      }
    });
    Collections.reverse(entityClouds);
  }

  @Override
  public List<CandidateAction> buildCandidateActions(final MapState mapState) {
    ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
    List<Monster> monsters = getAllMonsterWithinRadius(mapState.getMonsters(), mapState.getOwnBase(), 8000);
    List<EntityCloud<Monster>> entityClouds = kmeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
    sortByProximity(entityClouds, mapState);
    for (EntityCloud<Monster> entityCloud : entityClouds) {
      dealWithCloud(mapState, entityCloud, res);
    }
    return res;
  }

  private void dealWithCloud(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res) {
    List<Monster> monsters = entityCloud.getEntities();
    int bestHealth = Utils.getBestHealth(monsters);
    Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, mapState.getOwnBase());
    Pair<Heroe, Double> heroeAndDistance = Utils.findClosest(mapState.getHeroes(), entityCloud.getCenter());
    List<Heroe> otherHeroes = Utils.getOtherDefensiveHeroes(mapState, heroeAndDistance.getKey());
    double proximityFactor = Math.pow(3000/closestMonster.getValue(), 4);

    // eject
    if (heroeAndDistance.getValue() < Utils.WIND_RADIUS) {
      // value is 100 when entity is at 3000 of the base and bestHealth = 4. It decreases quickly as it goes away.
      double value = bestHealth*25*proximityFactor;
      res.add(new SpellWind(heroeAndDistance.getKey(), value, mapState.getEnemyBase(), ""));
    }

    // continue the fight (but it is less powerful). We follow, staying a bit closer to our base
    res.add(new Move(heroeAndDistance.getKey(), 90*proximityFactor,
            Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
                    mapState.getOwnBase()), ""));
    for (Heroe heroe : otherHeroes) {
      // propose to other defender to participate
      res.add(new Move(heroe, 45 * proximityFactor * 1000 / heroe.position.distance(mapState.getPointsOfInterest(heroe)),
              Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
                      mapState.getOwnBase()), ""));
    }

    // if it is an isolated monster, we can try to send it to the side, or making it join a group
    if (heroeAndDistance.getValue() < Utils.CONTROL_RADIUS && monsters.size() == 1 && monsters.get(0).isThreatForUs()) {
      dealWithIsolatedMonster(mapState, monsters.get(0), heroeAndDistance, proximityFactor, res);
    }
  }

  private void dealWithIsolatedMonster(MapState mapState, Monster monster, Pair<Heroe, Double> heroeAndDistance, double proximityFactor, ArrayList<CandidateAction> res) {
    // go away
    if (!monster.isTargettingOurBase()) {
      res.add(new SpellControl(heroeAndDistance.getKey(), monster,
              mapState.getEnemyBase(), 30*proximityFactor, ""));
    }
    // join a bigger group
    else if (monster.isTargettingOurBase()) {
      Pair<Monster, Double> closestFromIsolated = Utils.findClosest(mapState.getMonsters(), monster.position);
      if (closestFromIsolated.getValue() < Utils.MONSTER_SPEED*.9) {
        res.add(new SpellControl(heroeAndDistance.getKey(), monster,
                closestFromIsolated.getKey().position, 40*proximityFactor, ""));
      }
    }
  }

  private List<Monster> getAllMonsterWithinRadius(List<Monster> monsters, Vector referencePoint, double radius) {
    List<Monster> res = new ArrayList<>();
    for (Monster monster : monsters) {
      if (monster.position.sub(referencePoint).size() < radius) {
        res.add(monster);
      }
    }
    return res;
  }
}
