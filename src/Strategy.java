/*
 * 
 */
import java.util.List;

public abstract class Strategy {

  abstract public List<CandidateAction> buildCandidateActions(MapState mapState);

}
