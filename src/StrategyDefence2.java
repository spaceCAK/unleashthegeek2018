
/**
 *
 */
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StrategyDefence2 extends Strategy {

	private final KMeans<Monster> kmeans;

	public StrategyDefence2() {
		kmeans = new KMeans<Monster>();
	}

	private void sortByProximity(List<EntityCloud<Monster>> entityClouds, final MapState mapState) {
		Collections.sort(entityClouds, new Comparator<EntityCloud<Monster>>() {
			@Override
			public int compare(EntityCloud<Monster> o1, EntityCloud<Monster> o2) {
				return (int) (o1.getCenter().distance(mapState.getOwnBase())
						- o2.getCenter().distance(mapState.getOwnBase()));
			}
		});
		Collections.reverse(entityClouds);
	}

	@Override
	public List<CandidateAction> buildCandidateActions(final MapState mapState) {
		ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
		List<Monster> monsters = getAllMonsterWithinRadius(mapState.getMonsters(), mapState.getOwnBase(), 6700);
		List<EntityCloud<Monster>> entityClouds = kmeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
		for (EntityCloud<Monster> cloud : entityClouds) {
			System.err.println(cloud);
		}
		sortByProximity(entityClouds, mapState);
		for (EntityCloud<Monster> entityCloud : entityClouds) {
			dealWithCloud(mapState, entityCloud, res);
		}
		return res;
	}

	private void dealWithCloud(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res) {
		double mapStateValue = mapState.calculateValue();
		List<Monster> monsters = entityCloud.getEntities();
		Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, mapState.getOwnBase());
		Pair<Heroe, Double> heroeAndDistance = Utils.findClosest(mapState.getDefensiveHeroes(),
				entityCloud.getCenter());
		List<Heroe> otherHeroes = Utils.getOtherDefensiveHeroes(mapState, heroeAndDistance.getKey());
		double proximityFactor = Math.pow(3000 / closestMonster.getValue(), 4);

		// eject
		if (closestMonster.getKey().shieldLife == 0) {
			eject(mapState, res, heroeAndDistance, mapStateValue, mapState.getEnemyBase(),
							"fffou");
		}

		// continue the fight (but it is less powerful). We follow, staying a
		// bit closer to our base
		continueFight(mapState, entityCloud, res, heroeAndDistance, otherHeroes, proximityFactor, mapStateValue);

		// if it is an isolated monster, we can try to send it to the side, or
		// making it join a group
		if (heroeAndDistance.getValue() < Utils.CONTROL_RADIUS && monsters.size() == 1
				&& monsters.get(0).isThreatForUs()) {
			dealWithIsolatedMonster(mapState, monsters.get(0), heroeAndDistance, proximityFactor, res, mapStateValue);
		}
		// we try to group clusters in another way
		else {
			joinOtherCluster(mapState, entityCloud, res, mapStateValue, heroeAndDistance, proximityFactor);
		}
	}

	private void joinOtherCluster(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res,
			double mapStateValue, Pair<Heroe, Double> heroeAndDistance, double proximityFactor) {
		List<Monster> otherMonsters = new ArrayList<>();
		for (Monster candidate : mapState.getMonsters()) {
			if (!entityCloud.getEntities().contains(candidate)) {
				otherMonsters.add(candidate);
			}
		}
		Pair<Monster, Double> closest = Utils.findClosest(otherMonsters, entityCloud.getCenter());
		if (closest.getKey() != null) {
			eject(mapState, res, heroeAndDistance, mapStateValue,
					mapState.getEnemyBase().add(closest.getKey().position), "group");
		}
	}

	private void continueFight(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res,
			Pair<Heroe, Double> heroeAndDistance, List<Heroe> otherHeroes, double proximityFactor,
			double mapStateValue) {

		Vector centroid = Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
				entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
				mapState.getOwnBase());
		MapState newMapState = mapState.calculateMapStateMove(heroeAndDistance.getKey(), centroid);
		res.add(new Move(heroeAndDistance.getKey(), newMapState.calculateValue() - mapStateValue, centroid,
				"to death!"));
		for (Heroe heroe : otherHeroes) {
			// propose to other defender to participate
			newMapState = mapState.calculateMapStateMove(heroe, centroid);
			res.add(new Move(heroe, newMapState.calculateValue() - mapStateValue, centroid, "wait for me!"));
		}
	}

	private void eject(MapState mapState, ArrayList<CandidateAction> res,
			Pair<Heroe, Double> heroeAndDistance, double mapStateValue, Vector windDirection,
			String explanation) {
		if (heroeAndDistance.getValue() < Utils.WIND_RADIUS*.8) {
			// value is 100 when entity is at 3000 of the base and bestHealth =
			// 4. It decreases quickly as it goes away.
			MapState newMapState = mapState.calculateMapStateWind(heroeAndDistance.getKey(), mapState.getEnemyBase());
			res.add(new SpellWind(heroeAndDistance.getKey(), newMapState.calculateValue() - mapStateValue,
					windDirection, explanation));
		}
	}

	private void dealWithIsolatedMonster(MapState mapState, Monster monster, Pair<Heroe, Double> heroeAndDistance,
			double proximityFactor, ArrayList<CandidateAction> res, double mapStateValue) {
		// go away
		if (!monster.isTargettingOurBase()) {

			MapState newMapState = mapState.calculateMapStateControl(monster, mapState.getEnemyBase());
			res.add(new SpellControl(heroeAndDistance.getKey(), monster, mapState.getEnemyBase(),
					newMapState.calculateValue() - mapStateValue, "this way please"));
		}
		// join a bigger group
		else if (monster.isTargettingOurBase()) {
			if (monster.position.distance(mapState.getOwnBase()) > 5000) {

				Pair<Monster, Double> closestFromIsolated = Utils.findClosest(mapState.getMonsters(), monster.position);
				if (closestFromIsolated.getValue() < Utils.MONSTER_SPEED * .9) {
					MapState newMapState = mapState.calculateMapStateControl(monster,
							closestFromIsolated.getKey().position);
					res.add(new SpellControl(heroeAndDistance.getKey(), monster, closestFromIsolated.getKey().position,
							newMapState.calculateValue() - mapStateValue, "join 'em"));
				}
			}
		}
	}

	private List<Monster> getAllMonsterWithinRadius(List<Monster> monsters, Vector referencePoint, double radius) {
		List<Monster> res = new ArrayList<>();
		for (Monster monster : monsters) {
			if (monster.position.sub(referencePoint).size() < radius) {
				res.add(monster);
			}
		}
		return res;
	}
}
