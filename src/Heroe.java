
/**
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Heroe extends Entity {
	private MapState latestMapState;
	boolean isAttacker;

	Heroe(Scanner in, int id, MapState latestMapState, boolean isAttacker) {
		super(in);
		this.id = id;
		this.isAttacker = isAttacker;
	}

	List<Monster> getMonstersToAttack(List<Monster> monsters) {
		List<Monster> res = new ArrayList<>();
		for (Monster m : monsters) {
			double distanceToMonster = m.position.distance(this.position);
			if(distanceToMonster < 15*800){				
				res.add(m);
			}
		}
		return res;
	}

	static Monster getMostDangerousMonster(List<Monster> monsters) {
		Monster monster = null;
		double weight = 0;
		for (Monster m : monsters) {
			if (monster == null || weight < m.levelOfDanger) {
				monster = m;
				weight = monster.levelOfDanger;
			}
		}
		return monster;
	}

	public Heroe(Heroe toCopy) {
		super(toCopy);
	}

}
