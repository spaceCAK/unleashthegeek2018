import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TestKMeans {

  @Test
  public void testInsufficientEntities() {
    KMeans<Entity> kmeans = new KMeans<Entity>();
    Map<Vector, List<Entity>> clusters1 = kmeans.findClusters(new ArrayList<Entity>(), 10);
    assert clusters1.size() == 0;

    // when k > number of entities, then we reduce k to the number of entities
    Entity entity = new Entity(Vector.createCVector(154, 139));
    Map<Vector, List<Entity>> clusters2 = kmeans.findClusters(Arrays.asList(entity), 2);
    assert clusters2.size() == 1;
    assert clusters2.containsKey(entity.position);
    assert clusters2.get(entity.position).contains(entity);
  }

  @Test
  public void testK1() {
    KMeans<Entity> kmeans = new KMeans<Entity>();

    Entity entity = new Entity(Vector.createCVector(154, 139));
    Map<Vector, List<Entity>> clusters1 = kmeans.findClusters(Arrays.asList(entity), 1);
    Assert.assertEquals(1, clusters1.size());
    assert clusters1.containsKey(entity.position);
    assert clusters1.get(entity.position).size() == 1;
    assert clusters1.get(entity.position).contains(entity);

    Entity entity2 = new Entity(Vector.createCVector(100, 100));
    Map<Vector, List<Entity>> clusters2 = kmeans.findClusters(Arrays.asList(entity, entity2), 1);
    Assert.assertEquals(1, clusters2.size());
    Assert.assertEquals(2, clusters2.values().iterator().next().size());
  }

  @Test
  public void testK2() {
    KMeans<Entity> kmeans = new KMeans<Entity>();
    Entity entityA1 = new Entity(Vector.createCVector(10, 10));
    Entity entityA2 = new Entity(Vector.createCVector(8, 13));
    Entity entityA3 = new Entity(Vector.createCVector(9, 11));
    Entity entityB1 = new Entity(Vector.createCVector(20, 12));
    Map<Vector, List<Entity>> clusters = kmeans.findClusters(Arrays.asList(entityA1, entityB1, entityA2, entityA3), 2);
    Assert.assertEquals(2, clusters.size());
    boolean groupAFound = false;
    boolean groupBFound = false;
    for (Map.Entry<Vector, List<Entity>> cluster : clusters.entrySet()) {
      Vector center = cluster.getKey();
      List<Entity> elements = cluster.getValue();
      if (elements.contains(entityA1)) {
        groupAFound = true;
        assert elements.contains(entityA2);
        assert elements.contains(entityA3);
      } else if (elements.contains(entityB1)) {
        groupBFound = true;
      }
    }
    assert groupAFound && groupBFound;
  }

  @Test
  public void testFindKClusters() {
    Entity entityA1 = new Entity(Vector.createCVector(10, 10));
    Entity entityA2 = new Entity(Vector.createCVector(9, 11));
    Entity entityA3 = new Entity(Vector.createCVector(11, 9));
    Entity entityB1 = new Entity(Vector.createCVector(20, 12));
    Entity entityC1 = new Entity(Vector.createCVector(40, 12));
    Entity entityC2 = new Entity(Vector.createCVector(41, 12));
    List<EntityCloud<Entity>> clusters = (new KMeans<Entity>()).findKClusters(Arrays.asList(
            entityB1, entityA1, entityC1, entityA3, entityC2, entityA2), 2);
    Assert.assertEquals(3, clusters.size());
  }
}
