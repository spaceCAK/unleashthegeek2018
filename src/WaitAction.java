/**
 *
 */
public class WaitAction extends CandidateAction {

  public WaitAction(Heroe heroe, String explanation) {
    super(heroe, 0, 0, explanation);
  }

  @Override
  public boolean isSpell() {
    return false;
  }

  @Override
  public boolean isMove() {
    return true;
  }

  public void send() {
    System.out.println("WAIT" + " " + heroe.id + " " + explanation);
  }
}
