/**
 *
 */
import java.util.Scanner;

public class Entity implements Cloneable {
	int id;
	int type;
	Vector position;
	Vector speed;
	int shieldLife;
	int isControlled;
	int health;
	int nearBase;
	int threatFor;
	int targetOf = -1;

	Entity(Scanner in) {
		this.position = Vector.createCVector(in.nextInt(), in.nextInt());
		this.shieldLife = in.nextInt();
		this.isControlled = in.nextInt();
		this.health = in.nextInt();
		this.speed = Vector.createCVector(in.nextInt(), in.nextInt());
		this.nearBase = in.nextInt();
		this.threatFor = in.nextInt();
	}

	Entity(Vector position) {
		this.position = position;
	}

	public Entity(Entity toCopy) {
		position = toCopy.position.clone();
		id = toCopy.id;
		type = toCopy.type;
		speed = toCopy.speed.clone();
		shieldLife = toCopy.shieldLife;
		isControlled = toCopy.isControlled;
		health = toCopy.health;
		nearBase = toCopy.nearBase;
		threatFor = toCopy.threatFor;
		targetOf = toCopy.targetOf;
	}
	
	public Vector getFuturePosition(){
		return position.add(speed);
	}
}