/**
 * 
 * @author cpichery
 *
 */
public class Move extends CandidateAction {

	private Vector position;

	public Move(Heroe heroe, double value, Vector position, String explanation) {
		super(heroe, 0, value, explanation);
		this.position = position;
	}

	@Override
	public boolean isSpell() {
		return false;
	}

	@Override
	public boolean isMove() {
		return true;
	}

	public void send() {
		System.out.println("MOVE " + (int) position.getX() + " " + (int) position.getY() + " " + heroe.id + " " + explanation);
	}

}
