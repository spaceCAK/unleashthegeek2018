/**
 *
 */

import java.util.List;

public class EntityCloud<T extends Entity> {

  private final List<T> entities;
  private Vector center;
  private double diameter = 0;

  public EntityCloud(List<T> entities) {
    this.entities = entities;
    Vector[] points = new Vector[entities.size()];
    for (int i = 0 ; i < points.length ; i++) {
      points[i] = entities.get(i).position;
    }
    center = Vector.boxCenter(points);
    for (Entity entity : entities) {
      diameter = Math.max(diameter, center.distance(entity.position));
    }
  }

  public Vector getCenter() {
    return center;
  }

  public double getDiameter() {
    return diameter;
  }

  public List<T> getEntities() {
    return entities;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("cloud");
    for (Entity entity : entities) {
      builder.append(" " + entity.id);
    }
    return builder.toString();
  }
}
