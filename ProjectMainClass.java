import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.io.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Collection;
import javafx.util.Pair;
import java.util.ArrayList;
import java.math.*;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Scanner;
import java.util.*;




/**
 * 
 */
class Attack extends Strategy {

	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		List<CandidateAction> actions = new ArrayList<>();
		for (Heroe heroe : mapState.getHeroes()) {
			if (heroe.isAttacker) {
				List<Monster> monsters = heroe.getMonstersToAttack(mapState.getMonsters());
				int index = 0;
				for (Monster monster : monsters) {
						if(mapState.availableMana > 50 && monster.shieldLife == 0 && monster.willHit(5, mapState.getEnemyBase()) && monster.position.distance(heroe.position) < 2200){
							actions.add(new SpellShield(heroe, monster, 100, "Sayonara!"));
						}
						if (mapState.availableMana > 50 && monster.shieldLife == 0 && monster.position.distance(heroe.position) < 2200) {
							actions.add(new SpellControl(heroe, monster, mapState.getEnemyBase(), monster.health, "good boi"));
						}
						if (!monster.isTargettingEnemyBase()) {
							actions.add(new Move(heroe, monster.health - index * 2, monster.position, "ramdam"));
						}
					
					index++;
				}
			}
		}
		return actions;
	}

}


/*
 * 
 */
abstract class CandidateAction implements Comparable<CandidateAction> {

  protected final String explanation;
  Heroe heroe;
  double value;
  int manaCost;

  public CandidateAction(Heroe heroe, int manaCost, double value, String explanation) {
    if (heroe == null) throw new IllegalArgumentException("Action cannot be done on null heroe:" + this.getClass().getSimpleName());
    this.heroe = heroe;
    this.manaCost = manaCost;
    this.value = value;
    this.explanation = explanation;
  }

  public abstract boolean isSpell();
  public abstract boolean isMove();

  public int compareTo(CandidateAction otherCandidateAction) {
    return (int)(value - otherCandidateAction.value);
  }

  public abstract void send();
  
  public String toString(){
	  return this.getClass().getName() + " H" + (this.heroe == null ? null : this.heroe.id ) + " V" + this.value + " M" + this.manaCost + " " + explanation;
  }

}


/*
 * 
 */
class CandidateActionChooser {

	private final MapState mapState;
	private HashSet<Heroe> busyHeroes = new HashSet<>();

	CandidateActionChooser(MapState mapState) {
		this.mapState = mapState;
	}

	public List<CandidateAction> choose(List<CandidateAction> candidateActions) {
		Collections.sort(candidateActions);
		Collections.reverse(candidateActions);
		List<CandidateAction> res = new ArrayList<>();
		Iterator<CandidateAction> iterator = candidateActions.iterator();
		while (iterator.hasNext() && res.size() < 3) {
			proposeAction(iterator.next(), res);
		}
		return sortPerHeroe(res);
	}

	private List<CandidateAction> sortPerHeroe(List<CandidateAction> res) {
		List<CandidateAction> out = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			for (CandidateAction action : res) {
				if (action.heroe.id == i) {
					out.add(action);
					break;
				}
			}
		}
		return out;
	}

	private void proposeAction(CandidateAction action, List<CandidateAction> res) {
		if (action.manaCost > mapState.availableMana)
			return;
		if (action.heroe == null) {
			// no constraint on heroe
			res.add(action);
			mapState.availableMana -= action.manaCost;
		} else {
			if (!busyHeroes.contains(action.heroe)) {
				// heroe is available
				res.add(action);
				mapState.availableMana -= action.manaCost;
				busyHeroes.add(action.heroe);
			}
		}
	}
}


/**
 *
 */
class Enemy extends Entity {
	public Enemy(Scanner in, int id) {
		super(in);
		this.id = id;
	}

	public Enemy(Enemy toCopy) {
		super(toCopy);
	}

}


/**
 *
 */
class Entity implements Cloneable {
	int id;
	int type;
	Vector position;
	Vector speed;
	int shieldLife;
	int isControlled;
	int health;
	int nearBase;
	int threatFor;
	int targetOf = -1;

	Entity(Scanner in) {
		this.position = Vector.createCVector(in.nextInt(), in.nextInt());
		this.shieldLife = in.nextInt();
		this.isControlled = in.nextInt();
		this.health = in.nextInt();
		this.speed = Vector.createCVector(in.nextInt(), in.nextInt());
		this.nearBase = in.nextInt();
		this.threatFor = in.nextInt();
	}

	Entity(Vector position) {
		this.position = position;
	}

	public Entity(Entity toCopy) {
		position = toCopy.position.clone();
		id = toCopy.id;
		type = toCopy.type;
		speed = toCopy.speed.clone();
		shieldLife = toCopy.shieldLife;
		isControlled = toCopy.isControlled;
		health = toCopy.health;
		nearBase = toCopy.nearBase;
		threatFor = toCopy.threatFor;
		targetOf = toCopy.targetOf;
	}
	
	public Vector getFuturePosition(){
		return position.add(speed);
	}
}

/**
 *
 */
class EntityCloud<T extends Entity> {

  private final List<T> entities;
  private Vector center;
  private double diameter = 0;

  public EntityCloud(List<T> entities) {
    this.entities = entities;
    Vector[] points = new Vector[entities.size()];
    for (int i = 0 ; i < points.length ; i++) {
      points[i] = entities.get(i).position;
    }
    center = Vector.boxCenter(points);
    for (Entity entity : entities) {
      diameter = Math.max(diameter, center.distance(entity.position));
    }
  }

  public Vector getCenter() {
    return center;
  }

  public double getDiameter() {
    return diameter;
  }

  public List<T> getEntities() {
    return entities;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("cloud");
    for (Entity entity : entities) {
      builder.append(" " + entity.id);
    }
    return builder.toString();
  }
}


/**
 *
 */
class Heroe extends Entity {
	private MapState latestMapState;
	boolean isAttacker;

	Heroe(Scanner in, int id, MapState latestMapState, boolean isAttacker) {
		super(in);
		this.id = id;
		this.isAttacker = isAttacker;
	}

	List<Monster> getMonstersToAttack(List<Monster> monsters) {
		List<Monster> res = new ArrayList<>();
		for (Monster m : monsters) {
			double distanceToMonster = m.position.distance(this.position);
			if(distanceToMonster < 15*800){				
				res.add(m);
			}
		}
		return res;
	}

	static Monster getMostDangerousMonster(List<Monster> monsters) {
		Monster monster = null;
		double weight = 0;
		for (Monster m : monsters) {
			if (monster == null || weight < m.levelOfDanger) {
				monster = m;
				weight = monster.levelOfDanger;
			}
		}
		return monster;
	}

	public Heroe(Heroe toCopy) {
		super(toCopy);
	}

}


/*
 * 
 */
class HoldLine extends Strategy {

	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		List<CandidateAction> actions = new ArrayList<>();
		/**for (Heroe heroe : mapState.getHeroes()) {
			if (!heroe.isAttacker) {
				List<Monster> monsters = mapState.getMonsters();
				Monster monster = Heroe.getMostDangerousMonster(monsters);
				if (monster != null) {
					actions.add(new Move(heroe, monster.health, monster.position));
				}
				actions.add(new HoldPosition(heroe, mapState));
			}
		}
		 */
		return actions;
	}

}


/**
 *
 */
class HoldPosition extends CandidateAction {

	Vector position;

	public HoldPosition(Heroe heroe, MapState mapState, String explanation) {
		super(heroe, 0, heroe.position.distance(mapState.getPointsOfInterest(heroe))*.01, explanation);
		position = mapState.getPointsOfInterest(heroe);
	}

	@Override
	public boolean isSpell() {
		return false;
	}

	@Override
	public boolean isMove() {
		return true;
	}
	

	public void send() {
		System.out.println("MOVE " + (int) position.getX() + " " + (int) position.getY() + " " + heroe.id + " " + explanation);
	}

}


/**
 *
 */
class KMeans<T extends Entity> {

  public Map<Vector, List<T>> kmeansPlusPlus(List<T> entities, int k) {
    Map<Vector, List<T>> clusters = new HashMap<>();
    // we decide centroid #1
    clusters.put(entities.get(0).position, new ArrayList<T>());
    for (int i = 1 ; i < k ; i++) {
      // find centroid #k
      Vector candidate = null;
      double distanceToCandidate = -1;
      for (T entity : entities) {
        // find a candidate among entities
        Pair<Vector, Double> closestCenter = Utils.findClosestPosition(clusters.keySet(), entity.position);
        if (closestCenter.getValue() > distanceToCandidate) {
          candidate = entity.position;
          distanceToCandidate = closestCenter.getValue();
        }
      }
      if (candidate == null) throw new IllegalStateException("Why null in k-means++?");
      clusters.put(candidate, new ArrayList<T>());
    }
    return clusters;
  }

  private void buildClusters(List<T> entities, Map<Vector, List<T>> clusters) {
    for (T entity : entities) {
      Pair<Vector, Double> center = Utils.findClosestPosition(clusters.keySet(), entity.position);
      clusters.get(center.getKey()).add(entity);
    }
  }

  /**
   * return null if no need to improve further
   */
  private Map<Vector, List<T>> improveCenters(Map<Vector, List<T>> clusters) {
    double positionSplit = 0;
    Map<Vector, List<T>> newCenters = new HashMap<>();
    for (Map.Entry<Vector, List<T>> cluster : clusters.entrySet()) {
      List<T> clusterElements = cluster.getValue();
      Vector[] positions = new Vector[clusterElements.size()];
      for (int i = 0 ; i < clusterElements.size() ; i++) {
        positions[i] = clusterElements.get(i).position;
      }
      Vector newCenter = Vector.centroid(positions);
      positionSplit += newCenter.distance(cluster.getKey());
      newCenters.put(newCenter, new ArrayList<T>());
    }
    positionSplit /= clusters.size();
    if (positionSplit < 5) return null;
    return newCenters;
  }

  public Map<Vector, List<T>>  findClusters(List<T> entities, int k) {
    if (entities.isEmpty()) return new HashMap<>();
    Map<Vector, List<T>> newClusters = kmeansPlusPlus(entities, k);
    Map<Vector, List<T>> clusters = null;
    int counter = 0;
    do {
      clusters = newClusters;
      buildClusters(entities, clusters);
      newClusters = improveCenters(clusters);
      counter++;
    } while (newClusters != null && counter < 100);
    return clusters;
  }

  public List<EntityCloud<T>> findKClusters(List<T> entities, double maxAcceptedDiameter) {
    List<EntityCloud<T>> res;
    Map<Vector, List<T>> clusters;
    double maxDiameter;
    int k = 1;
    do {
      clusters = findClusters(entities, k);
      k++;
      maxDiameter = 0;
      res = new ArrayList<>(clusters.size());
      for (List<T> clusterElements : clusters.values()) {
        EntityCloud entityCloud = new EntityCloud<T>(clusterElements);
        res.add(entityCloud);
        maxDiameter = Math.max(maxDiameter, entityCloud.getDiameter());
      }
    } while (maxDiameter > maxAcceptedDiameter && k <= entities.size());
    return res;
  }

}


/*
 * 
 */
class MapState {
	private Vector basePosition;
	private List<Heroe> heroes;
	private List<Enemy> enemies;
	private List<Monster> monsters;
	public int availableMana;
	public int health;
	public int otherAvailableMana;
	public int otherHealth;

	public static Vector MAP_SIZE = Vector.createCVector(17630, 9000);
	public static int MONSTER_ATTACK_RADIUS = 5000;
	public static int MONSTER_DAMAGE_RADIUS = 300;
	public static int MONSTER_SPEED = 400;

	List<Vector> positions;

	public MapState(Vector basePosition) {
		this.basePosition = basePosition;
			positions = Arrays.asList(Vector.createCVector(13500, 5500), Vector.createCVector(3500, 2000),
					Vector.createCVector(2000, 3500), MAP_SIZE.sub(Vector.createCVector(13500, 5500)),
					MAP_SIZE.sub(Vector.createCVector(3500, 2000)), MAP_SIZE.sub(Vector.createCVector(2000, 3500)));
	}

	public Vector getPosition(int heroeId){
		return positions.get(heroeId);
	}
	public List<Heroe> getDefensiveHeroes() {
		return heroes.subList(1, 3);
	}

	public Heroe getAttackingHeroe() {
		return heroes.get(0);
	}

	public Vector getOwnBase() {
		return basePosition;
	}

	public Vector getEnemyBase() {
		if (basePosition.getX() == 0) {
			return MAP_SIZE;
		}
		return Vector.createCVector(0, 0);
	}

	public List<Heroe> getHeroes() {
		return heroes;
	}

	public void setHeroes(List<Heroe> heroes) {
		this.heroes = heroes;
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<Enemy> enemies) {
		this.enemies = enemies;
	}

	public List<Monster> getMonsters() {
		return monsters;
	}

	public void setMonsters(List<Monster> monsters) {
		this.monsters = monsters;
	}

	public MapState(MapState toCopy) {
		heroes = new ArrayList<>();
		for (Heroe heroe : toCopy.heroes) {
			heroes.add(new Heroe(heroe));
		}
		monsters = new ArrayList<>();
		for (Monster monster : toCopy.monsters) {
			monsters.add(new Monster(monster));
		}
		enemies = new ArrayList<>();
		for (Enemy enemy : toCopy.enemies) {
			enemies.add(new Enemy(enemy));
		}

		basePosition = toCopy.basePosition;
		availableMana = toCopy.availableMana;
		health = toCopy.health;
		positions = toCopy.positions;
	}
	
	

	public double calculateValue() {
		return calculateDefenseValue() + calculateAttackValue() + availableMana;
	}

	private double calculateAttackValue() {
		double value = 0;
		for (Monster m : monsters) {
			double monsterValue = m.health;
			if (m.willHit(15, getEnemyBase())) {
				monsterValue *= 5;
				if (m.shieldLife > 0) {
					monsterValue *= 10;
					if (m.willHit(m.shieldLife, getEnemyBase())) {
						monsterValue *= 2;
					}
				}
			}
			else{
				monsterValue += calculateDistancePoints(m.position.distance(getEnemyBase()), 15);
			}
			value += monsterValue;
		}
		Heroe heroe = getAttackingHeroe();
		double heroeValue = 0;
		heroeValue -= heroe.position.distance(getPointsOfInterest(heroe));
		for (Monster m : monsters) {
			double distance = heroe.position.distance(m.position);
			// don't consider this Monster if we can't reach it in less than 4
			// steps
			// don't consider this monster if it will hit the enemy
			if (distance > 10 * 800 || m.willHit(15, getEnemyBase())) {
				continue;
			}
			heroeValue += calculateDistancePoints(distance, 5);
		}
		value += heroeValue;

		return value;
	}

	private double calculateDistancePoints(double distance, double maxPoints) {
		return maxPoints - (distance / 800);
	}

	private double calculateDefenseValue() {
		double res = 0;
		// nb of clusters
		KMeans<Monster> kMeans = new KMeans<Monster>();
		List<EntityCloud<Monster>> pureDefenceClusters = kMeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
		// pureDefenceClustersNumber
		res += pureDefenceClusters.size();
		// windDefenceClustersNumber
		res += kMeans.findKClusters(monsters, Utils.WIND_RADIUS).size();

		for (EntityCloud<Monster> cluster : pureDefenceClusters) {
			res += computeClusterRisk(cluster);
		}
		// distance to preferred position
		res += computeKeepLineError();
		return -res;
	}

	private double computeClusterRisk(EntityCloud<Monster> cluster) {
		// health
		int bestHealth = Utils.getBestHealth(monsters);
		Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, getOwnBase());
		// defensive capability
		double heroeDistance = Utils.findClosest(getDefensiveHeroes(), closestMonster.getKey().position).getValue();
		// enemy attack capability
		double attackerDistance = Utils.findClosest(getEnemies(), cluster.getCenter()).getValue();

		double res = 0;
		res += Math.min(0, heroeDistance - Utils.ATTACK_RADIUS*.8)*.001;
		res += Utils.WIND_RADIUS*1.1 >= attackerDistance ? 1 : 0;
		res += Math.pow(3000 / closestMonster.getValue(), 4);
		res += Math.pow(2000 / closestMonster.getValue(), 4);
		return res*bestHealth;
	}

	private double computeKeepLineError() {
		List<Heroe> defensers = getDefensiveHeroes();
		double res = (Math.max(0, defensers.get(0).position.distance(defensers.get(1).position) - 3000))*.001;
		for (Heroe heroe : getHeroes()) {
			if (heroe.isAttacker) continue;
			res += Math.pow(heroe.position.distance(getPointsOfInterest(heroe))*.001, 2)*.5;
		}
		return res;
	}

	public MapState calculateMapStateShield(Entity entity) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (entity.id == m.id) {
				m.shieldLife = 12;
				break;
			}
		}
		for (Heroe h : copy.getHeroes()) {
			if (entity.id == h.id) {
				h.shieldLife = 12;
				break;
			}
		}
		for (Enemy e : copy.getEnemies()) {
			if (entity.id == e.id) {
				e.shieldLife = 12;
				break;
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateControl(Entity entity, Vector targetPosition) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (entity.id == m.id) {
				m.speed = targetPosition.sub(m.position).normalize(400);
				break;
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateWind(Heroe heroe, Vector windDirection) {
		MapState copy = new MapState(this);
		for (Monster m : copy.getMonsters()) {
			if (m.getFuturePosition().distance(heroe.position) < 1280 && m.shieldLife == 0) {
				m.position = m.position.add(windDirection.normalize(2200));
			}
		}
		for (Enemy e : copy.getEnemies()) {
			if (e.getFuturePosition().distance(heroe.position) < 1280 && e.shieldLife == 0) {
				e.position = e.position.add(windDirection.normalize(2200));
			}
		}
		for (Heroe h : copy.getHeroes()) {
			if (h.getFuturePosition().distance(heroe.position) < 1280 && h.shieldLife == 0 && h.id != heroe.id) {
				h.position = h.position.add(windDirection.normalize(2200));
			}
		}
		copy.availableMana -= 10;
		return copy;
	}

	public MapState calculateMapStateMove(Heroe heroe, Vector futurePosition) {
		MapState copy = new MapState(this);
		for (Heroe h : copy.getHeroes()) {
			if (h.id == heroe.id) {
				h.position = h.position
						.add(futurePosition.normalize(futurePosition.size() > 800 ? 800 : futurePosition.size()));
				break;
			}
		}
		return copy;
	}

	Vector getPointsOfInterest(Heroe heroe) {
		return getPosition(heroe.id);
	}
}


/**
 *
 */
class Monster extends Entity {
	double levelOfDanger;
	private MapState state;

	Monster(Scanner in, int id, MapState state) {
		super(in);
		this.id = id;
		this.state = state;
	}

	public void setLevelOfDanger(Vector basePosition) {
		levelOfDanger = 1;
		double distanceToBase = basePosition.distance(this.position);
		levelOfDanger /= distanceToBase / 1000;
		if (distanceToBase < 8000) {
			levelOfDanger *= 10;
		}
		if (distanceToBase < 5000) {
			levelOfDanger *= 10;
		}
	}

	public boolean isMonsterNearBorder() {
		return (this.position.getX() < 2000 || this.position.getX() > 17630 - 2000 || this.position.getY() < 2000
				|| this.position.getY() > 9000 - 2000);
	}

	public boolean isTargettingEnemyBase() {
		return nearBase == 1 && threatFor == 2;
	}

	public boolean isTargettingOurBase() {
		return nearBase == 1 && threatFor == 1;
	}

	public boolean isThreatForUs() {
		return threatFor == 1;
	}

	public boolean isThreatForEnemy() {
		return threatFor == 2;
	}

	public Monster(Monster toCopy) {
		super(toCopy);
		levelOfDanger = toCopy.levelOfDanger;
	}

	public boolean willHit(int iteration, Vector pos) {
		for(int i = 1; i < iteration + 1; i++){
			Vector futurePos = this.position.add(this.speed.scalar(i));
			if(futurePos.getX() > 0 && futurePos.getY() > 0 && futurePos.getY() < 9000 && futurePos.getX() < 17360){
				if(futurePos.distance(pos) < 5000){
					return true;
				}
			}
			else{
				return false;
			}
		}
		return false;
	}
}

/**
 * 
 * @author cpichery
 *
 */
class Move extends CandidateAction {

	private Vector position;

	public Move(Heroe heroe, double value, Vector position, String explanation) {
		super(heroe, 0, value, explanation);
		this.position = position;
	}

	@Override
	public boolean isSpell() {
		return false;
	}

	@Override
	public boolean isMove() {
		return true;
	}

	public void send() {
		System.out.println("MOVE " + (int) position.getX() + " " + (int) position.getY() + " " + heroe.id + " " + explanation);
	}

}


/**
*
*/
class Player {

	public static MapState latestMapState;

	public static void main(String args[]) throws IOException {
		Scanner in = new Scanner(System.in);
		latestMapState = new MapState(Vector.createCVector(in.nextInt(), in.nextInt()));

		int heroesPerPlayer = in.nextInt();

		// game loop
		while (true) {
			for (int i = 0; i < 2; i++) {
				if (i == 0) {
					latestMapState = new MapState(latestMapState.getOwnBase());
					latestMapState.health = in.nextInt();
					latestMapState.availableMana = in.nextInt();
				} else {
					latestMapState.otherHealth = in.nextInt();
					latestMapState.otherAvailableMana = in.nextInt();
				}
			}
			int PlayerCount = in.nextInt();
			List<Monster> monsters = new ArrayList<>();
			List<Heroe> heroes = new ArrayList<>();
			List<Enemy> enemies = new ArrayList<>();
			boolean isAttacker = true;

			for (int i = 0; i < PlayerCount; i++) {
				int id = in.nextInt();
				int type = in.nextInt();
				switch (type) {
				case 0:
					monsters.add(new Monster(in, id, latestMapState));
					break;
				case 1:
					// only the 1st hero is attacker
					heroes.add(new Heroe(in, id, latestMapState, isAttacker));
					isAttacker = false;
					break;
				default:
					enemies.add(new Enemy(in, id));
					break;
				}
			}
			latestMapState.setEnemies(enemies);
			latestMapState.setMonsters(monsters);
			latestMapState.setHeroes(heroes);

			act(latestMapState);

			if (false)
				break;
		}
	}

	private static void act(MapState mapState) {
		for (Monster monster : mapState.getMonsters()) {
			monster.setLevelOfDanger(latestMapState.getOwnBase());
		}
		// find candidate actions
		List<Strategy> strategies = Arrays.asList(/*new StrategyConcludeSpider(), */
						new StrategyHold2(),
						new StrategyAttack2(),
						new StrategyDefence2());
		List<CandidateAction> candidateActions = new ArrayList<>();
		for (Strategy strategy : strategies) {
			candidateActions.addAll(strategy.buildCandidateActions(mapState));
		}

		// decide which actions to take
		List<CandidateAction> actionsToTake = (new CandidateActionChooser(mapState)).choose(candidateActions);
		for(CandidateAction action : actionsToTake){
			action.send();
		}
	}

}

/**
 *
 */
class PointOfInterest {
	Vector position;
	int targetOf;
	
	PointOfInterest(Vector position, int targetOfHeroe){
		this.position = position;
		this.targetOf = targetOfHeroe;
	}

}


/*
 * 
 */
abstract class Spell extends CandidateAction {
  Entity target;

  Spell(Heroe heroe, Entity target, double value, String explanation) {
    super(heroe, 10, value, explanation);
    this.target = target;
  }

  public boolean isSpell() {return true;}
  public boolean isMove() {return false;}
}


/*
 * 
 */
class SpellControl extends Spell {

  Vector direction;

  SpellControl(Heroe hero, Entity targetMonster, Vector direction, double value, String explanation) {
    super(hero, targetMonster, value, explanation);
    this.direction = direction;
  }

  public void send() {
		System.out.println(
				"SPELL CONTROL " + target.id + " " + (int) direction.getX() + " " + (int) direction.getY() + " " + heroe.id + " " + explanation);
  }

}


/**
 * 
 * @author cpichery
 *
 */
class SpellShield extends Spell {

	SpellShield(Heroe heroe, Entity target, double value, String explanation) {
		super(heroe, target, value, explanation);
	}

	public void send() {
		System.out.println("SPELL SHIELD " + target.id + " " + heroe.id + " " + explanation);
	}

}


/**
 * 
 * @author cpichery
 *
 */
class SpellWind extends Spell {

	private Vector direction;

	SpellWind(Heroe heroe, double value, Vector direction, String explanation) {
		super(heroe, null, value, explanation);
		this.direction = direction;
	}

	public void send() {
		System.out.println("SPELL WIND " + (int) direction.getX() + " " + (int) direction.getY() + " " + heroe.id + " " + explanation);
	}

}


/*
 * 
 */
abstract class Strategy {

  abstract public List<CandidateAction> buildCandidateActions(MapState mapState);

}


/**
 * 
 */
class StrategyAttack2 extends Strategy {

	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		List<CandidateAction> actions = new ArrayList<>();
		double mapStateValue = mapState.calculateValue();
		Heroe heroe = mapState.getAttackingHeroe();
		List<Monster> monsters = heroe.getMonstersToAttack(mapState.getMonsters());
		for (Monster monster : monsters) {
			// use spells only if no shield
			if (monster.shieldLife == 0) {
				
				if (monster.willHit(10, mapState.getEnemyBase()) && monster.position.distance(heroe.position) < 2200) {
					MapState newMapState = mapState.calculateMapStateShield(monster);
					System.err.println(newMapState.calculateValue() - mapStateValue);
					actions.add(new SpellShield(heroe, monster, newMapState.calculateValue() - mapStateValue, "Sayonara!"));
				}
				if (monster.getFuturePosition().distance(heroe.position) < 2200 && mapState.availableMana > 20 && monster.position.distance(mapState.getEnemyBase()) < 8000) {
					MapState newMapState = mapState.calculateMapStateControl(monster, mapState.getEnemyBase());
					actions.add(new SpellControl(heroe, monster, mapState.getEnemyBase(),
							1500, "good boi"));
				}
				if (monster.getFuturePosition().distance(heroe.position) < 1280) {
					MapState newMapState = mapState.calculateMapStateWind(heroe, mapState.getEnemyBase());
					actions.add(new SpellWind(heroe, newMapState.calculateValue() - mapStateValue,
							mapState.getEnemyBase(), "go!go!"));
				}
			}

			if (!monster.willHit(10, mapState.getEnemyBase())) {
				MapState newMapState = mapState.calculateMapStateMove(heroe, monster.getFuturePosition());
				actions.add(new Move(heroe, newMapState.calculateValue() - mapStateValue, monster.position, "ramdam"));
			}
		}
		return actions;
	}

}


/*
 * 
 */
class StrategyConcludeSpider extends Strategy {

  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    List<CandidateAction> candidates = new ArrayList<>();
    for (Monster monster : mapState.getMonsters()) {
      if (monster.shieldLife > 0) continue;
      double distanceToEnemy = monster.position.distance(mapState.getEnemyBase());


    }
    return candidates;
  }
}


/**
 *
 */
class StrategyDefence extends Strategy {

  private final KMeans<Monster> kmeans;

  public StrategyDefence() {
    kmeans = new KMeans<Monster>();
  }

  private void sortByProximity(List<EntityCloud<Monster>> entityClouds, final MapState mapState) {
    Collections.sort(entityClouds, new Comparator<EntityCloud<Monster>>() {
      @Override
      public int compare(EntityCloud<Monster> o1, EntityCloud<Monster> o2) {
        return (int) (o1.getCenter().distance(mapState.getOwnBase()) - o2.getCenter().distance(mapState.getOwnBase()));
      }
    });
    Collections.reverse(entityClouds);
  }

  @Override
  public List<CandidateAction> buildCandidateActions(final MapState mapState) {
    ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
    List<Monster> monsters = getAllMonsterWithinRadius(mapState.getMonsters(), mapState.getOwnBase(), 8000);
    List<EntityCloud<Monster>> entityClouds = kmeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
    sortByProximity(entityClouds, mapState);
    for (EntityCloud<Monster> entityCloud : entityClouds) {
      dealWithCloud(mapState, entityCloud, res);
    }
    return res;
  }

  private void dealWithCloud(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res) {
    List<Monster> monsters = entityCloud.getEntities();
    int bestHealth = Utils.getBestHealth(monsters);
    Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, mapState.getOwnBase());
    Pair<Heroe, Double> heroeAndDistance = Utils.findClosest(mapState.getHeroes(), entityCloud.getCenter());
    List<Heroe> otherHeroes = Utils.getOtherDefensiveHeroes(mapState, heroeAndDistance.getKey());
    double proximityFactor = Math.pow(3000/closestMonster.getValue(), 4);

    // eject
    if (heroeAndDistance.getValue() < Utils.WIND_RADIUS) {
      // value is 100 when entity is at 3000 of the base and bestHealth = 4. It decreases quickly as it goes away.
      double value = bestHealth*25*proximityFactor;
      res.add(new SpellWind(heroeAndDistance.getKey(), value, mapState.getEnemyBase(), ""));
    }

    // continue the fight (but it is less powerful). We follow, staying a bit closer to our base
    res.add(new Move(heroeAndDistance.getKey(), 90*proximityFactor,
            Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
                    mapState.getOwnBase()), ""));
    for (Heroe heroe : otherHeroes) {
      // propose to other defender to participate
      res.add(new Move(heroe, 45 * proximityFactor * 1000 / heroe.position.distance(mapState.getPointsOfInterest(heroe)),
              Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
                      mapState.getOwnBase()), ""));
    }

    // if it is an isolated monster, we can try to send it to the side, or making it join a group
    if (heroeAndDistance.getValue() < Utils.CONTROL_RADIUS && monsters.size() == 1 && monsters.get(0).isThreatForUs()) {
      dealWithIsolatedMonster(mapState, monsters.get(0), heroeAndDistance, proximityFactor, res);
    }
  }

  private void dealWithIsolatedMonster(MapState mapState, Monster monster, Pair<Heroe, Double> heroeAndDistance, double proximityFactor, ArrayList<CandidateAction> res) {
    // go away
    if (!monster.isTargettingOurBase()) {
      res.add(new SpellControl(heroeAndDistance.getKey(), monster,
              mapState.getEnemyBase(), 30*proximityFactor, ""));
    }
    // join a bigger group
    else if (monster.isTargettingOurBase()) {
      Pair<Monster, Double> closestFromIsolated = Utils.findClosest(mapState.getMonsters(), monster.position);
      if (closestFromIsolated.getValue() < Utils.MONSTER_SPEED*.9) {
        res.add(new SpellControl(heroeAndDistance.getKey(), monster,
                closestFromIsolated.getKey().position, 40*proximityFactor, ""));
      }
    }
  }

  private List<Monster> getAllMonsterWithinRadius(List<Monster> monsters, Vector referencePoint, double radius) {
    List<Monster> res = new ArrayList<>();
    for (Monster monster : monsters) {
      if (monster.position.sub(referencePoint).size() < radius) {
        res.add(monster);
      }
    }
    return res;
  }
}


/**
 *
 */
class StrategyDefence2 extends Strategy {

	private final KMeans<Monster> kmeans;

	public StrategyDefence2() {
		kmeans = new KMeans<Monster>();
	}

	private void sortByProximity(List<EntityCloud<Monster>> entityClouds, final MapState mapState) {
		Collections.sort(entityClouds, new Comparator<EntityCloud<Monster>>() {
			@Override
			public int compare(EntityCloud<Monster> o1, EntityCloud<Monster> o2) {
				return (int) (o1.getCenter().distance(mapState.getOwnBase())
						- o2.getCenter().distance(mapState.getOwnBase()));
			}
		});
		Collections.reverse(entityClouds);
	}

	@Override
	public List<CandidateAction> buildCandidateActions(final MapState mapState) {
		ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
		List<Monster> monsters = getAllMonsterWithinRadius(mapState.getMonsters(), mapState.getOwnBase(), 8000);
		List<EntityCloud<Monster>> entityClouds = kmeans.findKClusters(monsters, Utils.ATTACK_RADIUS);
		for (EntityCloud<Monster> cloud : entityClouds) {
			System.err.println(cloud);
		}
		sortByProximity(entityClouds, mapState);
		for (EntityCloud<Monster> entityCloud : entityClouds) {
			dealWithCloud(mapState, entityCloud, res);
		}
		return res;
	}

	private void dealWithCloud(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res) {
		double mapStateValue = mapState.calculateValue();
		List<Monster> monsters = entityCloud.getEntities();
		int bestHealth = Utils.getBestHealth(monsters);
		Pair<Monster, Double> closestMonster = Utils.findClosest(monsters, mapState.getOwnBase());
		Pair<Heroe, Double> heroeAndDistance = Utils.findClosest(mapState.getDefensiveHeroes(),
				entityCloud.getCenter());
		List<Heroe> otherHeroes = Utils.getOtherDefensiveHeroes(mapState, heroeAndDistance.getKey());
		double proximityFactor = Math.pow(3000 / closestMonster.getValue(), 4);

		// eject
		eject(mapState, res, bestHealth, heroeAndDistance, mapStateValue, mapState.getEnemyBase(),
				"fffou");

		// continue the fight (but it is less powerful). We follow, staying a
		// bit closer to our base
		continueFight(mapState, entityCloud, res, heroeAndDistance, otherHeroes, proximityFactor, mapStateValue);

		// if it is an isolated monster, we can try to send it to the side, or
		// making it join a group
		if (heroeAndDistance.getValue() < Utils.CONTROL_RADIUS && monsters.size() == 1
				&& monsters.get(0).isThreatForUs()) {
			dealWithIsolatedMonster(mapState, monsters.get(0), heroeAndDistance, proximityFactor, res, mapStateValue);
		}
		// we try to group clusters in another way
		else {
			joinOtherCluster(mapState, entityCloud, res, mapStateValue, bestHealth, heroeAndDistance, proximityFactor);
		}
	}

	private void joinOtherCluster(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res,
			double mapStateValue, int bestHealth, Pair<Heroe, Double> heroeAndDistance, double proximityFactor) {
		List<Monster> otherMonsters = new ArrayList<>();
		for (Monster candidate : mapState.getMonsters()) {
			if (!entityCloud.getEntities().contains(candidate)) {
				otherMonsters.add(candidate);
			}
		}
		Pair<Monster, Double> closest = Utils.findClosest(otherMonsters, entityCloud.getCenter());
		if (closest.getKey() != null) {
			eject(mapState, res, bestHealth, heroeAndDistance, mapStateValue,
					mapState.getEnemyBase().add(closest.getKey().position), "group");
		}
	}

	private void continueFight(MapState mapState, EntityCloud<Monster> entityCloud, ArrayList<CandidateAction> res,
			Pair<Heroe, Double> heroeAndDistance, List<Heroe> otherHeroes, double proximityFactor,
			double mapStateValue) {

		Vector centroid = Vector.centroid(entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
				entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(), entityCloud.getCenter(),
				mapState.getOwnBase());
		MapState newMapState = mapState.calculateMapStateMove(heroeAndDistance.getKey(), centroid);
		res.add(new Move(heroeAndDistance.getKey(), newMapState.calculateValue() - mapStateValue, centroid,
				"to death!"));
		for (Heroe heroe : otherHeroes) {
			// propose to other defender to participate
			newMapState = mapState.calculateMapStateMove(heroe, centroid);
			res.add(new Move(heroe, newMapState.calculateValue() - mapStateValue, centroid, "wait for me!"));
		}
	}

	private void eject(MapState mapState, ArrayList<CandidateAction> res, int bestHealth,
			Pair<Heroe, Double> heroeAndDistance, double mapStateValue, Vector windDirection,
			String explanation) {
		if (heroeAndDistance.getValue() < Utils.WIND_RADIUS*.8) {
			// value is 100 when entity is at 3000 of the base and bestHealth =
			// 4. It decreases quickly as it goes away.
			MapState newMapState = mapState.calculateMapStateWind(heroeAndDistance.getKey(), mapState.getEnemyBase());
			res.add(new SpellWind(heroeAndDistance.getKey(), newMapState.calculateValue() - mapStateValue,
					windDirection, explanation));
		}
	}

	private void dealWithIsolatedMonster(MapState mapState, Monster monster, Pair<Heroe, Double> heroeAndDistance,
			double proximityFactor, ArrayList<CandidateAction> res, double mapStateValue) {
		// go away
		if (!monster.isTargettingOurBase()) {

			MapState newMapState = mapState.calculateMapStateControl(monster, mapState.getEnemyBase());
			res.add(new SpellControl(heroeAndDistance.getKey(), monster, mapState.getEnemyBase(),
					newMapState.calculateValue() - mapStateValue, "this way please"));
		}
		// join a bigger group
		else if (monster.isTargettingOurBase()) {
			if (monster.position.distance(mapState.getOwnBase()) > 5000) {

				Pair<Monster, Double> closestFromIsolated = Utils.findClosest(mapState.getMonsters(), monster.position);
				if (closestFromIsolated.getValue() < Utils.MONSTER_SPEED * .9) {
					MapState newMapState = mapState.calculateMapStateControl(monster,
							closestFromIsolated.getKey().position);
					res.add(new SpellControl(heroeAndDistance.getKey(), monster, closestFromIsolated.getKey().position,
							newMapState.calculateValue() - mapStateValue, "join 'em"));
				}
			}
		}
	}

	private List<Monster> getAllMonsterWithinRadius(List<Monster> monsters, Vector referencePoint, double radius) {
		List<Monster> res = new ArrayList<>();
		for (Monster monster : monsters) {
			if (monster.position.sub(referencePoint).size() < radius) {
				res.add(monster);
			}
		}
		return res;
	}
}


/*
 * 
 */
class StrategyEjectSpider extends Strategy {


  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    return new ArrayList<>();
  }
}


/**
 *
 */
class StrategyHold extends Strategy {
  @Override
  public List<CandidateAction> buildCandidateActions(MapState mapState) {
    ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
    for (Heroe heroe : mapState.getHeroes()) {
      res.add(new HoldPosition(heroe, mapState, ""));
    }
    return res;
  }
}


/**
 *
 */
class StrategyHold2 extends Strategy {
	@Override
	public List<CandidateAction> buildCandidateActions(MapState mapState) {
		ArrayList<CandidateAction> res = new ArrayList<CandidateAction>();
		for (Heroe heroe : mapState.getHeroes()) {
			MapState newMapState = mapState.calculateMapStateMove(heroe, mapState.getPosition(heroe.id));
			res.add(new Move(heroe, newMapState.calculateValue() - mapState.calculateValue(),
					mapState.getPosition(heroe.id), "hold the line!"));
		}
		return res;
	}
}


/**
 * 
 */
class Utils {

  public static int ATTACK_RADIUS = 800;
  public static int WIND_RADIUS = 1280;
  public static int CONTROL_RADIUS = 2200;
  public static int SHIELD_RADIUS = 2200;
  public static int MONSTER_SPEED = 400;

  static public int getBestHealth(List<Monster> monsters) {
    int bestHealth = 0;
    for (Monster monster : monsters) {
      bestHealth = Math.max(monster.health, bestHealth);
    }
    return bestHealth;
  }

  static public <T extends Entity> Pair<T, Double> findClosest(Collection<T> entities, Vector referencePosition) {
    T candidate = null;
    double distance = 100000;
    double candidateDistance;
    for (T entity : entities) {
      candidateDistance = entity.position.distance(referencePosition);
      if (candidateDistance < distance) {
        distance = candidateDistance;
        candidate = entity;
      }
    }
    return new Pair<>(candidate, distance);
  }

  static public Pair<Vector, Double> findClosestPosition(Collection<Vector> positions, Vector referencePosition) {
    Vector candidate = null;
    double distance = 100000;
    double candidateDistance;
    for (Vector position : positions) {
      candidateDistance = position.distance(referencePosition);
      if (candidateDistance < distance) {
        distance = candidateDistance;
        candidate = position;
      }
    }
    return new Pair<>(candidate, distance);
  }

  static public List<Heroe> getOtherDefensiveHeroes(MapState mapState, Heroe heroe) {
    List<Heroe> res = new ArrayList<>();
    for (Heroe candidateHeroe : mapState.getHeroes()) {
      if (candidateHeroe != heroe && !candidateHeroe.isAttacker) {
        res.add(candidateHeroe);
      }
    }
    return res;
  }

}


/**
 *
 */
class Vector implements Cloneable {

  private static int MAX = 1000000;

  public enum Coord {X, Y;}

  private double x; double y; double r; double theta;
  private Vector(double x, double y, double r, double theta) {this.x = x; this.y = y; this.r = r; this.theta = theta;}
  double getX() {return x;} double getY() {return y;} double getR() {return r;} double getTheta() {return theta;}
  double getCoord(Coord coord) {switch(coord) {case X: return x; case Y: return y;} throw new IllegalArgumentException("Coord cannot be null");}
  double size()               {return Math.abs(r);}
  Vector add(Vector o)        {return createCVector(x+o.x, y+o.y);}
  Vector scalar(double l)     {return createCVector(l*x, l*y);}
  double scalar(Vector o)     {return x*o.x+y*o.y;}
  double cross(Vector o)      {return x*o.y-y*o.x;}
  Vector sub(Vector o)        {return add(o.scalar(-1));}
  Vector normalize(double s)  {return createPVector(s, theta);}
  Vector rotateRadians(double radians) {return createPVector(r, theta + radians);}
  static Vector createCVector(double x, double y) {return new Vector(x, y, Math.sqrt(x*x+y*y), (Math.sqrt(x*x+y*y)<0.01)?0:Math.atan2(y,x));}
  static Vector createPVector(double r, double theta) {return new Vector(r*Math.cos(theta), r*Math.sin(theta), r, theta);}
  static Vector nullVector()  {return new Vector(0,0,0,0);}
  @Override
  public String toString() {return "("+x+","+y+")";}
  public String toPString() {return "("+r+","+theta+")";}
  public double distance(Vector otherPos){return this.sub(otherPos).size();}

  @Override
  public Vector clone() {
    return Vector.createCVector(getX(), getY());
  }

  public static Vector centroid(Vector... points) {
    Vector res = nullVector();
    for (int i = 0 ; i < points.length ; i++) {
      res = res.add(points[i]);
    }
    return res.scalar(1./points.length);
  }

  public static Vector boxCenter(Vector... points) {
    if (points.length == 0) return nullVector();
    double minX = MAX;
    double maxX = -MAX;
    double minY = MAX;
    double maxY = -MAX;
    for (int i = 0 ; i < points.length ; i++) {
      Vector point = points[i];
      minX = Math.min(minX, point.x);
      maxX = Math.max(maxX, point.x);
      minY = Math.min(minY, point.y);
      maxY = Math.max(maxY, point.y);
    }
    return createCVector(.5*(minX+maxX), .5*(minY+maxY));
  }

}


/**
 *
 */
class WaitAction extends CandidateAction {

  public WaitAction(Heroe heroe, String explanation) {
    super(heroe, 0, 0, explanation);
  }

  @Override
  public boolean isSpell() {
    return false;
  }

  @Override
  public boolean isMove() {
    return true;
  }

  public void send() {
    System.out.println("WAIT" + " " + heroe.id + " " + explanation);
  }
}


