import os, sys

python_version = sys.version[0]

folderPath = './src/'
# mainFile is the file containing the main class.
mainFile = 'ProjectMainClass.java'
# exclude these files from the project.
excluded = []

def printSafe(s):
	s = str(s).encode('ascii', 'ignore')
	s = s.decode('ascii')
	print(s)

def getName(file):
    return file[file.rfind('/')+1:]
    
def isMain(file):
    global mainFile
    return getName(file) == mainFile
    
def getFirstClassInterface(s):
    index = s.find('public') + 7
    return index
    
def getImports(file):
    f = open(file)
    s = f.read()
    f.close()
    
    s = s[:getFirstClassInterface(s)]
    lines = s.split('\n')
    impts = []
    for line in lines:
        if line.find('import java') != -1: 
            impts.append(line)
    return impts

def processFile(file, isPublic):
    f = open(file)
    s = f.read()
    f.close()
    
    startIndex = getFirstClassInterface(s)
    classCommentIndex = s.find('/*')
    commentEndIndex = s.find('*/', classCommentIndex)
    if classCommentIndex < startIndex < commentEndIndex:
        startIndex = commentEndIndex + getFirstClassInterface(s[commentEndIndex:])
    
    publicString = ''
    if isPublic:
        publicString = 'public '
        
    if classCommentIndex > startIndex:
        print('Missing class comment for ' + file)
        return publicString + s[startIndex:]
    else:
        return s[classCommentIndex:commentEndIndex] + '*/\n' + publicString + s[startIndex:]


if __name__ == '__main__':
    if python_version == '2':
        allFileNames = os.walk(folderPath).next()[2]
    else:
        allFileNames = os.walk(folderPath).__next__()[2]

    allFiles = [os.path.join(folderPath, filename) for filename in allFileNames if filename.endswith('.java') and filename not in excluded and not filename.lower().startswith('test')]
    print ("all files:")
    print (allFiles)
    
    
    jImports = set()
    for file in allFiles:
        jImports = jImports.union(getImports(file))
    
    s = []
    for imp in jImports:
        s.append(imp + '\n')
    
    s.append('\n\n')
    for file in allFiles:
        if isMain(file):
            s.append(processFile(file, True))
    
    s.append('\n\n')
    for file in allFiles:
        if not isMain(file):
            pro = processFile(file, False)
            s.append(pro)
            print('- combine ' + getName(file))
            s.append('\n\n')
    
    s = ''.join(s)
    
    f = open(mainFile, 'w+')
    f.write(s)
    f.close()
    #printSafe(s)
print ('Combination complete')
import clipboard
with open(mainFile, 'r') as f :
    clipboard.copy(f.read())